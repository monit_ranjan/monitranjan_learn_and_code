#importing requests for api call
import requests, json

def get_response(blog_name, api_parameter):
    URL = 'https://'+blog_name+'.tumblr.com/api/read/json'
    response = []
    while api_parameter['num'] > 0:
        response.append(requests.get(url=URL, params=api_parameter).text)
        api_parameter['start'] += 50
        api_parameter['num'] -= 50
    return response


def convert_to_valid_json(response):
    index = 0
    json_response = []
    while index < len(response):
        #getting start and end index of substring which is valid json
        start_index = response[index].find('{')
        end_index = response[index].rindex('}')
        #removing invalid substring from start and end
        response[index] = response[index][start_index:end_index+1]
        json_response.append(json.loads(response[index]))
        index += 1
    return json_response


def generate_api_parameter(min_range, max_range):
    return {
        'type':'photo',
        'num':max_range - min_range+1,
        'start':min_range-1,
    }
 