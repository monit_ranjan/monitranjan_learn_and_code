import random

class FoodSystem:

    def __init__(self,quantity):
        self.total_quantity = quantity
    
    def supplyFood(self, required_food):
        if (required_food<=self.total_quantity):
            self.total_quantity = self.total_quantity - required_food
            return required_food
        supplied_food = self.total_quantity
        self.total_quantity = 0
        return supplied_food

class Person:

    def __init__(self,available_food_quantity):
        self.available_food_quantity = FoodSystem(available_food_quantity)
    
    def feed(self,food):
        return self.available_food_quantity.supplyFood(food)

class Animal:

    def __init__(self,animal_food_requirement):
        self.animal_food_requirement = animal_food_requirement

    def ask_for_food(self, person):
        provided_food = person.feed(self.animal_food_requirement)
        if  provided_food == self.animal_food_requirement:
            return "Animal has been feeded"
        elif provided_food == 0:
            return "Food not available"
        return "Required food not available I have feeded " + str(provided_food) + " units only"

animal = Animal(15)
person = Person(100)

while True:
    is_hungry = random.randint(0,10)
    if is_hungry <5:
        print(animal.ask_for_food(person))
    else:
        print("Animal is not hungry")
    print("Do you want to try again? (Y/N)")
    while True:
        user_response=input()
        if user_response=='Y' or user_response=='N':
            break
        else:
            print("Enter valid response")

    if user_response =='Y':
        print("Please provide food requirement of animal in numerical units")
        while True:
            try:
                animal_food_requirement = int(input())
                break
            except:
                print("Please provide integer value")
        animal = Animal(animal_food_requirement)
    elif(user_response =='N'):
        print("Thank you!!")
        break
    