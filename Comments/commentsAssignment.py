#importing required functions from apiOperations
from apiOperations import getResponse,convertToValidJson,generateApiParameter
def blogNameInput():
    blogNameInputMessage = "enter the Tumblr blog name : "
    blogName = input(blogNameInputMessage)
    return blogName

def rangeInput():
    rangeInputMessage = "enter the range : "
    range = input(rangeInputMessage)
    rangeEndpoints = range.split(' ')
    return int(rangeEndpoints[0]),int(rangeEndpoints[2])

def isValidRange(jsonResponse,minRange,maxRange):
    if minRange < 1 or maxRange > jsonResponse[0]['posts-total']:
        print("Enter Valid Range")
        return False
    return True

def isBlogAvailable(jsonResponse):
    if jsonResponse[0]['posts-total']:
        return True
    print('There is no such blog')
    return False

def isValid(response,minRange,maxRange):
    if isBlogAvailable(response) and isValidRange(response,minRange,maxRange):
        return True
    return False

def showBasicInformation(jsonResponse):
    print('title : ',jsonResponse[0]['tumblelog']['title'])
    print('name : ',jsonResponse[0]['tumblelog']['name'])
    print('description : ',jsonResponse[0]['tumblelog']['description'])
    print ('no of post : ',jsonResponse[0]['posts-total'])

def showImageUrls(minrange,maxrange,response,blogName):
    highestImageQuality = 'photo-url-1280'
    index = 0
    postNumber = minrange
    while index < len(response):
        for post in response[index]['posts']:
            #printing image url which has image quality of 1280
            print(postNumber,' : ',post[highestImageQuality])
            for photo in post['photos']:
                #checking the duplication of image url
                if(photo[highestImageQuality] != post[highestImageQuality]):
                    print('     ',photo[highestImageQuality])
            postNumber += 1
        index += 1

def validResponse(blogName,apiParameter):
    response = getResponse(blogName,apiParameter)
    JsonResponse = convertToValidJson(response)
    return JsonResponse

def showDetails(JsonResponse,minRange,maxRange,blogName):
    if isValid(JsonResponse,minRange,maxRange):
        showBasicInformation(JsonResponse)
        showImageUrls(minRange,maxRange,JsonResponse,blogName)
    
def main():
    blogName = blogNameInput()
    minRange,maxRange = rangeInput()
    apiParameter = generateApiParameter(minRange,maxRange)
    JsonResponse = validResponse(blogName,apiParameter)
    showDetails(JsonResponse,minRange,maxRange,blogName)

main()
