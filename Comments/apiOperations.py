#importing requests for api call
import requests,json
def getResponse(blogName,apiParameter):
    URL = 'https://'+blogName+'.tumblr.com/api/read/json'
    response = []
    while apiParameter['num'] > 0:
        response.append(requests.get(url=URL,params=apiParameter).text)
        apiParameter['start'] += 50
        apiParameter['num'] -= 50
    return response

def convertToValidJson(response):
    index = 0
    jsonResponse = []
    while index < len(response):
        #getting start and end index of substring which is valid json
        startIndex = response[index].find('{')
        endIndex = response[index].rindex('}')
        #removing invalid substring from start and end
        response[index] = response[index][startIndex:endIndex+1]
        jsonResponse.append(json.loads(response[index]))
        index += 1
    return jsonResponse

def generateApiParameter(minRange , maxRange):
    return {
    'type':'photo',
    'num':maxRange - minRange+1,
    'start':minRange-1
    }
 