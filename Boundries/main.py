import place
key = '4aad6b807b5f4001a9398feca12d2ba3'

def main():
	address = place.Place(key)
	placeName = input("Enter Place:-")
	url = address.getUrl(placeName)
	responseObj = address.getResponse(url)
	jsonObj = address.convertIntoJson(responseObj)
	try:
		location = address.getLongitudeLatitude(jsonObj)
	except:
		print('Enter valid Place')
		exit()
	print('Latitude:', location[0])
	print('Longitude:', location[1])
	print('Address is:- ', address.getLocation(jsonObj))

main()