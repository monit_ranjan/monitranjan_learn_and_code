import requests
import json

class Place:
    def __init__(self, key):
        self.key = key

    def getUrl(self, place):
        URL = "https://api.opencagedata.com/geocode/v1/json?q=" + place + "&key=" + self.key + "&language=en&pretty=1"
        return URL
    
    def getResponse(self, URL):
	    return requests.get(url=URL)
    
    def convertIntoJson(self, responseObj):
	    return json.loads(responseObj.text)
    
    def getLongitudeLatitude(self, jsonData):
        placeDetail = []
        placeDetail.append(jsonData['results'][0]['bounds']['northeast']['lat'])
        placeDetail.append(jsonData['results'][0]['bounds']['northeast']['lng'])
        return placeDetail

    def getLocation(self, jsonData):
        address = jsonData['results'][0]['formatted']
        return address