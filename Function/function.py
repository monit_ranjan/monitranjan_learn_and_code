import random
def checkNumber(inputNumber):
    if inputNumber.isdigit() and 1<= int(inputNumber) <=100:
        return True
    else:
        return False

def guessNumber(numberToGuess):
    guessedNumber=False
    userGuess=input("Guess a number between 1 and 100:")
    numberOfGuesses=0
    while not guessedNumber:
        if not checkNumber(userGuess):
            userGuess=input("I wont count this one Please enter a number between 1 to 100")
            continue
        else:
            numberOfGuesses+=1
            userGuess=int(userGuess)

        if userGuess<numberToGuess:
            userGuess=input("Too low. Guess again")
        elif userGuess>numberToGuess:
            userGuess=input("Too High. Guess again")
        else:
            print("You guessed it in",numberOfGuesses,"guesses!")
            guessedNumber=True


def main():
 numberToGuess=random.randint(1,100)
 guessNumber(numberToGuess)

main()
