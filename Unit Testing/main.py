def no_Of_divisor(number):
    count=0
    for iteration in range(1,number+1):
        if(number%iteration==0):
            count+=1
    return count 


if __name__ == "__main__" :
    user_input=int(input("Please enter the number\n"))

    number_with_same_divisor=0
    for iterate in range(1,user_input):
        if(no_Of_divisor(iterate)==no_Of_divisor(iterate+1)):
            number_with_same_divisor+=1
    print (number_with_same_divisor)