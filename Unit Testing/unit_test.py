import main
import unittest

class TestCases(unittest.TestCase):

    def test_case_when_float_value_is_passed_raise_exception(self):
        self.assertRaises(Exception, main.no_Of_divisor, 1.76)

    def test_case_when_string_value_is_passed_raise_exception(self):
        self.assertRaises(Exception,  main.no_Of_divisor, 'zero')
    
    def test_case_for_negative(self):
        self.assertFalse(main.no_Of_divisor(3)<0)
    
    def test_case_for_positive(self):
        self.assertTrue(main.no_Of_divisor(100)>0)
        
unittest.main()